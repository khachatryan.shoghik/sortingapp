package com.example;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class AppTest {
    private String[] input;

    private String[] output;

    @Parameterized.Parameters
    static public Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new String[]{"1"}, new String[]{"1"}},
                {new String[]{"2", "1", "-2", "6", "3", "3", "9", "7", "8", "6"},
                        new String[]{"-2", "1", "2", "3", "3", "6", "6", "7", "8", "9"}}
        });
    }

    public AppTest(String[] input, String[] output) {
        this.input = input;
        this.output = output;
    }

    @Test
    public void testValidInput() {
        App.sort(input);
        Assert.assertArrayEquals(output, input);
    }
}

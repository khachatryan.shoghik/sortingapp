package com.example;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

/**
 * Bad cases for soring app.
 */
@RunWith(Parameterized.class)
public class BadCasesTest {
    private String[] input;


    @Parameterized.Parameters
    static public Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new String[]{}},
                {new String[]{"2", "1", "-2", "6", "3", "3", "9", "7", "8", "6", "11"}}
        });
    }

    public BadCasesTest(String[] input) {
        this.input = input;
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidInput() {
        App.sort(input);
    }
}


package com.example;

import java.util.Arrays;

import static java.lang.System.out;

/**
 * Input integers array with app and output will be sorted array
 */
public class App {
    /**
     * The main function get String array, convert to integer array,
     * then input not numeric throw NumberFormatException.
     *
     * @param args String[] number1 number2 ... format
     */
    public static void main(String[] args) {
        sort(args);

        System.out.println("Sorted:");
        Arrays.stream(args).forEach(x -> System.out.print(x + " "));
    }

    static public void sort(String[] args) {
        if (args.length == 0 || args.length > 10) {
            throw new IllegalArgumentException("Input should be (0,10] integer arguments.");
        }

        int[] numbers = new int[args.length];

        for (int i = 0; i < args.length; i++) {
            numbers[i] = Integer.parseInt(args[i]);
        }

        Arrays.sort(numbers);

        for (int i = 0; i < numbers.length; i++) {
            args[i] = String.valueOf(numbers[i]);
        }
    }

    /**
     * This function get integer array and sort that array
     *
     * @param array unsorted integer array
     */
    static private void sort(int[] array) {
        if (array == null) {
            throw new IllegalArgumentException("Array is NUll");
        }

        int lastIndex = array.length;

        for (int j = 0; j < lastIndex - 1; j++) {
            for (int i = j + 1; i < lastIndex; i++) {
                if (array[j] > array[i]) {
                    int tmp = array[j];
                    array[j] = array[i];
                    array[i] = tmp;
                }
            }
        }
    }
}

# SortingApp 
* Create a Maven project and specify its GAV settings, encoding, language level, etc.
* Write the code implementing the app specification.
* Configure the Maven project to build a runnable jar containing application and its dependencies.
* Share the project using a public GitLab repository.
* Submit a link to your repository.